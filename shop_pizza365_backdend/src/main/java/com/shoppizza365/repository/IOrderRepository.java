package com.shoppizza365.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.shoppizza365.model.*;

public interface IOrderRepository extends JpaRepository<COrder, Long> {
	@Query(value = "SELECT * FROM orders  WHERE status like %:status%", nativeQuery = true)
	List<COrder> findOrderByStatus(@Param("status") String status);
	
	@Query(value = "SELECT * FROM orders  WHERE status like %:status%", nativeQuery = true)
	List<COrder> findOrderByStatusPageable(@Param("status") String status,Pageable pageable);
	
	@Query(value = "SELECT * FROM orders  WHERE status like %:status% ORDER BY status DESC", nativeQuery = true)
	List<COrder> findOrderByStatusDESC(@Param("status") String status,Pageable pageable);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE orders SET status = :status WHERE status IS NULL", nativeQuery = true)
	int updateStatus(@Param("status") String status);

}
