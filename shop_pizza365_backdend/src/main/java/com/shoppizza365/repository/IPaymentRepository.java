package com.shoppizza365.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.shoppizza365.model.*;

public interface IPaymentRepository extends JpaRepository<CPayment, Long> {
	@Query(value = "SELECT * FROM payments  WHERE check_number like %:checknumber%", nativeQuery = true)
	List<CPayment> findPaymentBycheckNumber(@Param("checknumber") String checknumber);
	
	@Query(value = "SELECT * FROM payments  WHERE check_number like %:checknumber%", nativeQuery = true)
	List<CPayment> findPaymentBycheckNumberPageable(@Param("checknumber") String checknumber,Pageable pageable);
	
	@Query(value = "SELECT * FROM payments  WHERE check_number like %:checknumber% ORDER BY check_number DESC", nativeQuery = true)
	List<CPayment> findPaymentBycheckNumberDESC(@Param("checknumber") String checknumber,Pageable pageable);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE payments SET check_number = :checknumber WHERE check_number IS NULL", nativeQuery = true)
	int updateCheckNumber(@Param("checknumber") String checknumber);
}
