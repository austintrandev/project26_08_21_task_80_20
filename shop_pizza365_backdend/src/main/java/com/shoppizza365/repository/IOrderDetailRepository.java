package com.shoppizza365.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.shoppizza365.model.COrderDetail;

public interface IOrderDetailRepository extends JpaRepository<COrderDetail,Long> {
	@Query(value = "SELECT * FROM order_details  WHERE quantity_order >= :quantity", nativeQuery = true)
	List<COrderDetail> findOrderDetailByQuantityOrder(@Param("quantity") long quantity);
	
	@Query(value = "SELECT * FROM order_details  WHERE quantity_order >= :quantity", nativeQuery = true)
	List<COrderDetail> findOrderDetailByQauntityOrderPageable(@Param("quantity") long quantity,Pageable pageable);
	
	@Query(value = "SELECT * FROM order_details  WHERE quantity_order >= :quantity ORDER BY quantity_order DESC", nativeQuery = true)
	List<COrderDetail> findOrderDetailByQuantityOrderDESC(@Param("quantity") long quantity,Pageable pageable);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE order_details SET quantity_order = :quantity WHERE quantity_order = 0", nativeQuery = true)
	int updateQuantityOrder(@Param("quantity") long quantity);
}
