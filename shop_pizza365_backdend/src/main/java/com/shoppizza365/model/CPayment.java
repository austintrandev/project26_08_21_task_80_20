package com.shoppizza365.model;

import java.util.Date;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.*;

@Entity
@Table(name = "payments")
public class CPayment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	@JsonIgnore
	private CCustomer customer;
	
	@Transient
	private long customerId;

	@Column(name = "check_number")
	private String checkNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "payment_date", nullable = true)
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date paymentDate;

	@Column(name = "amount")
	private float ammount;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@JsonIgnore
	public CCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(CCustomer customer) {
		this.customer = customer;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public float getAmmount() {
		return ammount;
	}

	public void setAmmount(float ammount) {
		this.ammount = ammount;
	}

	public long getCustomerId() {
		return customer.getId();
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
}
