package com.shoppizza365.model;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="orders")
public class COrder {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "order_date", nullable = true)
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date orderDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "required_date", nullable = true)
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date requiredDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "shipped_date", nullable = true)
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date shippedDate;
	
	@Column(name="status")
	private String status;
	
	@Column(name="comments")
	private String comments;
	
	@ManyToOne
	@JsonIgnore
	private CCustomer customer;
	
	@Transient
	private String customerName;

	@OneToMany(targetEntity = COrderDetail.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "order_id")
	private List<COrderDetail> orderDetail;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}

	public Date getShippedDate() {
		return shippedDate;
	}

	public void setShippedDate(Date shippedDate) {
		this.shippedDate = shippedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@JsonIgnore
	public CCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(CCustomer customer) {
		this.customer = customer;
	}

	@JsonIgnore
	public List<COrderDetail> getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(List<COrderDetail> orderDetail) {
		this.orderDetail = orderDetail;
	}

	public String getCustomerName() {
		return customer.getFirstName() + " " + customer.getLastName();
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
}
