package com.shoppizza365.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="order_details")
public class COrderDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	@JsonIgnore
	private COrder order;
	
	@Transient
	private long orderId;
	
	
	@ManyToOne
	@JsonIgnore
	private CProduct product;
	
	@Transient
	private long productId;

	@Column(name = "quantity_order")
	private long quantityOrder;

	@Column(name = "price_each")
	private float priceEach;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public COrder getOrder() {
		return order;
	}

	public void setOrder(COrder order) {
		this.order = order;
	}

	public long getQuantityOrder() {
		return quantityOrder;
	}

	public void setQuantityOrder(long quantityOrder) {
		this.quantityOrder = quantityOrder;
	}

	public float getPriceEach() {
		return priceEach;
	}

	public void setPriceEach(float priceEach) {
		this.priceEach = priceEach;
	}

	public CProduct getProduct() {
		return product;
	}

	public void setProduct(CProduct product) {
		this.product = product;
	}

	public long getOrderId() {
		return order.getId();
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getProductId() {
		return product.getId();
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}
}
