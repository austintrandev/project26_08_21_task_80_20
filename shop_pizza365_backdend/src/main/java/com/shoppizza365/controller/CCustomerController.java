package com.shoppizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import com.shoppizza365.model.*;
import com.shoppizza365.repository.*;

@RestController
public class CCustomerController {
	@Autowired
	private ICustomerRepository customerRepository;
	
	@CrossOrigin
	@GetMapping("/customers")
	public ResponseEntity<List<CCustomer>> getAllCustomers() {
		try {
			List<CCustomer> pCustomers = new ArrayList<CCustomer>();

			customerRepository.findAll().forEach(pCustomers::add);

			return new ResponseEntity<>(pCustomers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/customer/{id}")
	public ResponseEntity<CCustomer> getCustomerById(@PathVariable("id") long id) {
		Optional<CCustomer> customerData = customerRepository.findById(id);
		if (customerData.isPresent()) {
			return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PostMapping("/customer/create")
	public ResponseEntity<Object> createCustomer(@RequestBody CCustomer pCustomer) {
		try {
			CCustomer newCustomer = new CCustomer();
			newCustomer.setAddress(pCustomer.getAddress());
			newCustomer.setCity(pCustomer.getCity());
			newCustomer.setCountry(pCustomer.getCountry());
			newCustomer.setCreditLimit(pCustomer.getCreditLimit());
			newCustomer.setFirstName(pCustomer.getFirstName());
			newCustomer.setLastName(pCustomer.getLastName());
			newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
			newCustomer.setPostalCode(pCustomer.getPostalCode());
			newCustomer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
			newCustomer.setState(pCustomer.getState());
			CCustomer savedCustomer = customerRepository.save(newCustomer);
			return new ResponseEntity<>(newCustomer, HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
		}
	}

	@CrossOrigin
	@PutMapping("/customer/update/{id}")
	public ResponseEntity<Object> updateCustomerById(@PathVariable("id") long id, @RequestBody CCustomer pCustomer) {
		Optional<CCustomer> customerData = customerRepository.findById(id);
		if (customerData.isPresent()) {
			CCustomer newCustomer = customerData.get();
			newCustomer.setAddress(pCustomer.getAddress());
			newCustomer.setCity(pCustomer.getCity());
			newCustomer.setCountry(pCustomer.getCountry());
			newCustomer.setCreditLimit(pCustomer.getCreditLimit());
			newCustomer.setFirstName(pCustomer.getFirstName());
			newCustomer.setLastName(pCustomer.getLastName());
			newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
			newCustomer.setPostalCode(pCustomer.getPostalCode());
			newCustomer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
			newCustomer.setState(pCustomer.getState());
			CCustomer savedCustomer = customerRepository.save(newCustomer);
			try {
				return new ResponseEntity<>(savedCustomer, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Customer:" + e.getCause().getCause().getMessage());
			}

		} else {
			// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified Customer: " + id + "  for update.");
		}
	}

	@CrossOrigin
	@DeleteMapping("/customer/delete/{id}")
	public ResponseEntity<CCustomer> deleteCustomerById(@PathVariable("id") long id) {
		try {
			customerRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/customer/first_or_lastname/{name}")
		public ResponseEntity<List<CCustomer>> findCustomerByFirstOrLastName(@PathVariable String name) {
			try {
				List<CCustomer> vCustomer = customerRepository.findCustomerByFirstOrLastName(name);
				return new ResponseEntity<>(vCustomer, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);	
		}
	}

	@GetMapping("/customer/city_or_state/{name}/{start}/{end}")
	public ResponseEntity<List<CCustomer>> findCustomerByCityOrState(@PathVariable String name,
			@PathVariable int start, @PathVariable int end) {
		try {
			List<CCustomer> vCustomer = customerRepository.findCustomerByCityOrState(name,
					PageRequest.of(start, end));
			return new ResponseEntity<>(vCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/customer/country/{name}/{start}/{end}")
	public ResponseEntity<List<CCustomer>> findCustomerByCountry(@PathVariable String name,
			@PathVariable int start, @PathVariable int end) {
		try {
			List<CCustomer> vCustomer = customerRepository.findCustomerByCountryDESC(name,
					PageRequest.of(start, end));
			return new ResponseEntity<>(vCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin
	@PutMapping("/customer/update_for_country/{country}")
	public int updateCountry(@PathVariable("country") String country) {
		return customerRepository.updateCountry(country);
	}
	
	
}
