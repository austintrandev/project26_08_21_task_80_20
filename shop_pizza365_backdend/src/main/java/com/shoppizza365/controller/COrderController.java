package com.shoppizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.shoppizza365.model.*;
import com.shoppizza365.repository.*;
@RestController
public class COrderController {
	@Autowired
	private IOrderRepository orderRepository;

	@Autowired
	private ICustomerRepository customerRepository;
	
	@CrossOrigin
	@GetMapping("/orders")
	public ResponseEntity<List<COrder>> getAllOrders() {
		try {
			List<COrder> pOrders = new ArrayList<COrder>();

			orderRepository.findAll().forEach(pOrders::add);

			return new ResponseEntity<>(pOrders, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/order/{id}")
	public ResponseEntity<COrder> getOrderById(@PathVariable("id") long id) {
		Optional<COrder> orderData = orderRepository.findById(id);
		if (orderData.isPresent()) {
			return new ResponseEntity<>(orderData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	
	@CrossOrigin
	@GetMapping("/customer/{customer_id}/order")
	public List<COrder> getOrderByCustomerId(@PathVariable Long customer_id) {
		if (customerRepository.findById(customer_id).isPresent())
			return customerRepository.findById(customer_id).get().getOrder();
		else
			return null;
	}
	
	@CrossOrigin
	@PostMapping("/order/create/{id}")
	public ResponseEntity<Object> createOrder(@PathVariable("id") Long id, @RequestBody COrder pOrder) {
		Optional<CCustomer> customerData = customerRepository.findById(id);
		if (customerData.isPresent()) {
			try {
				COrder newOrder = new COrder();
				newOrder.setComments(pOrder.getComments());
				newOrder.setOrderDate(pOrder.getOrderDate());
				newOrder.setRequiredDate(pOrder.getRequiredDate());
				newOrder.setShippedDate(pOrder.getShippedDate());
				newOrder.setStatus(pOrder.getStatus());
				newOrder.setCustomer(customerData.get());
				COrder savedOrder = orderRepository.save(newOrder);
				return new ResponseEntity<>(savedOrder, HttpStatus.CREATED);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/order/update/{id}")
	public ResponseEntity<Object> updateOrderById(@PathVariable("id") long id, @RequestBody COrder pOrder) {
		Optional<COrder> orderData = orderRepository.findById(id);
		if (orderData.isPresent()) {
			COrder newOrder = orderData.get();
			newOrder.setComments(pOrder.getComments());
			newOrder.setOrderDate(pOrder.getOrderDate());
			newOrder.setRequiredDate(pOrder.getRequiredDate());
			newOrder.setShippedDate(pOrder.getShippedDate());
			newOrder.setStatus(pOrder.getStatus());
			COrder savedOrder = orderRepository.save(newOrder);
			try {
				return new ResponseEntity<>(savedOrder, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Order:" + e.getCause().getCause().getMessage());
			}

		} else {
			// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified Order: " + id + "  for update.");
		}
	}

	@CrossOrigin
	@DeleteMapping("/order/delete/{id}")
	public ResponseEntity<COrder> deleteOrderById(@PathVariable("id") long id) {
		try {
			orderRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/order/status/{status}")
	public ResponseEntity<List<COrder>> findOrderByStatus(@PathVariable String status) {
		try {
			List<COrder> vOrder = orderRepository.findOrderByStatus(status);
			return new ResponseEntity<>(vOrder, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/order/status_pageable/{status}/{start}/{end}")
	public ResponseEntity<List<COrder>> findOrderByStatusPageable(@PathVariable String status, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<COrder> vOrder = orderRepository.findOrderByStatusPageable(status, PageRequest.of(start, end));
			return new ResponseEntity<>(vOrder, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/order/status_desc/{status}/{start}/{end}")
	public ResponseEntity<List<COrder>> findOrderByStatusDESC(@PathVariable String status, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<COrder> vOrder = orderRepository.findOrderByStatusDESC(status, PageRequest.of(start, end));
			return new ResponseEntity<>(vOrder, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/order/update_for_status/{status}")
	public int updateStatus(@PathVariable("status") String status) {
		return orderRepository.updateStatus(status);
	}
}
